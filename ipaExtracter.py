﻿#coding=utf-8
import os
import sys
import getopt
import zipfile

"""
######## 模块说明书 ########
"""	
__doc__ = ''


"""
######## 配置 ########
"""
CONFIG = dict(
	#IPA_FILE = 'ahup.ipa',
	ALLOWED_FILE_EXT = ['.png', '.jpg', '.mp3', '.wav', '.m4a'] # 允许的文件格式
)


class IPAExtracter:
	"""
	解剖IPA文件
	"""
	def __init__(self, ipa_file):
		
		self.ipa_file = ipa_file
		self.zip_file = zipfile.ZipFile( ipa_file,  mode='r')
		
	def extract_img(self):
		for file_fullname in self.zip_file.namelist():
			
			# 获取目录、文件名
			file_path, file_name = os.path.split( file_fullname )
			
			# 短命、扩展名
			file_shortname, file_ext = os.path.splitext( file_name )
			
			# 开始转存文件
			# 确定不是文件夹? 
			if not os.path.isdir(file_fullname):
				# 扩展名符合?
				if file_ext in CONFIG['ALLOWED_FILE_EXT']:
					
					# 目标存放文件目录
					#des_file_path = os.path.join( CONFIG['IPA_FILE'], file_path )  # 无法生效在win
					des_file_path = '%s_resources/%s' % ( self.ipa_file, file_path )
					
					# 创建目录 有Error 183  TODO
					try:
						os.makedirs( des_file_path )   # BUG HERE??
					except:
						pass
					
					# 开始写文件
					io = open( os.path.join( des_file_path, file_name ), 'wb')
					
					io.write( 
							self.zip_file.read( file_fullname )  	# 存放在哪里?
							)
					io.close()
					
					print u'释放文件: %s' % file_fullname

					
	
""""
######## 图形界面从这里开始 ########
""""
import Tkinter 

class TkApp(Tkinter.Frame):
	"""
	图形界面开始
	"""
	def __init__(self, master=None):
		Tkinter.Frame.__init__(self, master)
		self.pack()
		self.addWidgets()
	
	def addWidgets(self):
		# 退出按钮
		self.QUIT_BUTTON = Tkinter.Button(self, text=u'退出')
		self.QUIT_BUTTON['command'] = self.quit
		self.QUIT_BUTTON.pack({'side':'right'})
	
	
if __name__ == '__main__':
	"""
	
	"""
	# ####			#### #
	# #### 脚本模式 #### #
	# #### 			#### #
	
	# 如果有参数，进入脚本模式
	if len(sys.argv[1:]) is not 0: # 参数list长度不等于0
		# 短: h无参数 ,  f有参数,  
		# 长: help无参数， file有参数
		# 一下获得2个list, 其中opts是有用额， args是多余的未能解析的
		opts, args = getopt.getopt( sys.argv[1:], 'hf:', ['file=','help']  ) # 解析短参数和长参数
		
		# 读取opts开始
		for opt, arg in opts:
			if opt in ('-h', '--help'):
				print 'print help'
				exit(0) # 无错误退出
			elif opt in ('-f', '--file'):
				# 传入ipa文件地址，释放ipa文件
				print u'开始释放IPA文件: %s' % arg 
				ipa_file = IPAExtracter( arg )
				ipa_file.extract_img()
				print u'完成释放IPA文件: %s' % arg
				exit(0)
	else:
		# 没有参数，图形界面模式
		print 'GUI Mode start'
		
		root = Tkinter.Tk()
		app = TkApp(master=root)
		app.mainloop()
		root.quit()
		
		
		